<?php

use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;

// DIC configuration
$container = $app->getContainer();

// database
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};


// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//FrontEnd View
$container['view'] = function ($c) {
    $view = new Slim\Views\Twig($c['settings']['template_path'],[
        'debug'=>true
        //'cache'=>$c['settings']['template_path'].DS.'tmp'.DS.cache
    ]);
    // Resize live image to base64
    $_base64 = new Twig_SimpleFilter('imagebase64',function($string,$width,$height){
        //Verifica se a imagem existe
        $ch = curl_init($string);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        
        curl_close($ch);
        
        if($status) {
            $image_resize = new \App\Helpers\Image\ImageResize($string);
            //$image_resize->crop($width,$height);
            $image_resize->crop($width, $height, true, \App\Helpers\Image\ImageResize::CROPTOP);
            return $image_resize->base64();
        }else{
            $image_resize = new \App\Helpers\Image\ImageResize(WWW_ROOT.DS.'img'.DS.'img-indisponivel.jpg');
            //$image_resize->crop($width,$height);
            $image_resize->crop($width, $height, true, \App\Helpers\Image\ImageResize::CROPTOP);
            return $image_resize->base64();
        }
    });
    
    //Data em pt_BR
    $_data_pt_BR = new Twig_SimpleFilter('datePTBR',function ($date,$format = '%d de %B de %Y'){
        return  utf8_encode(strftime($format,strtotime($date)));
    });

    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    $view->addExtension(new \Twig_Extension_Debug());
    $view->addExtension(new \App\Helpers\TwigHTMLStatic($router, $uri,$c)); //HTML helpers
    $view->addExtension(new Bes\Twig\Extension\MobileDetectExtension()); //Mobile detect
    $view->getEnvironment()->addFilter($_base64);
    $view->getEnvironment()->addFilter($_data_pt_BR);
    return $view;
};


//Corrige slim3 para funcionar em subdiretorio
$container['environment'] = function () {
    $scriptName = $_SERVER['SCRIPT_NAME'];
    $_SERVER['REAL_SCRIPT_NAME'] = $scriptName;
    $_SERVER['SCRIPT_NAME'] = dirname(dirname($scriptName)) . '/' . basename($scriptName);
    return new Slim\Http\Environment($_SERVER);
};

// Controller
$container['Controller'] = function ($c) {
    return new \App\Controller($c);
};

//Página não encontrada
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withRedirect($c->get('router')->pathFor('erro404'));
    };
};

//Proibido o acesso!
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withRedirect($c->get('router')->pathFor('erro405'));
    };
};

