<?php

namespace App;

use Cake;

class App
{
    public $View;
    public $ViewAdmin;
    public $_title;

    public function __construct($container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->db = $container['db'];
        $this->view = $container['view']; 
    }

    //Salva a photo vinda de base64
    public function save_photo_base64($path,$name, $image64) {
        @mkdir($path, 0777,true);
        $file = $path. $name;
        $photobase = explode(',', $image64);
        $photo = base64_decode($photobase[1]);

        if(file_put_contents($file, $photo)){
            if(file_exists($file)) {
                //get dimensions of the original image
                @list($width_org, $height_org) = getimagesize($file);

                //get image coords
                if ($width_org === $height_org) {
                    $square = $width_org;
                    $offsetX = $offsetY = 0;
                } else if ($width_org < $height_org) {
                    $square = $width_org;
                    $offsetX = 0;
                    $offsetY = ($height_org - $width_org) / 2;
                } else {
                    $square = $height_org;              // $square: square side length
                    $offsetX = ($width_org - $height_org) / 2;  // x offset based on the rectangle
                    $offsetY = 0;
                }

                //define the final size of the cropped image
                $width_new = $width_org;
                $height_new = $height_org;

                //crop and resize image
                @$newImage = imagecreatetruecolor($width_new, $height_new);
                @$fileType = mime_content_type($file);
                switch ($fileType) {
                    case "image/gif":
                        $source = imagecreatefromgif($file);
                        break;
                    case "image/pjpeg":
                    case "image/jpeg":
                    case "image/jpg":
                        $source = imagecreatefromjpeg($file);
                        break;
                    case "image/png":
                    case "image/x-png":
                        $source = imagecreatefrompng($file);
                        break;
                }

                @imagecopyresampled($newImage, $source, 0, 0, 0, 0, $width_new, $height_new, $width_org, $height_org);

                switch ($fileType) {
                    case "image/gif":
                        imagegif($newImage, $file);
                        break;
                    case "image/pjpeg":
                    case "image/jpeg":
                    case "image/jpg":
                        imagejpeg($newImage, $file, 5);
                        break;
                    case "image/png":
                    case "image/x-png":
                        imagepng($newImage, $file);
                        break;
                }
                imagedestroy($newImage);
            }


            return true;
        }else{
            return false;
        }
    }

    //Verificação de URL
    protected function url_verify($string, $model, $id = '')
    {
        $slugfy = $this->slugify($string, '-');
        //Caso não tenha passado o id
        if ($id == "") {
            $url_result_verify = $model->where('url', $slugfy)->first();
            if (count($url_result_verify) >= 1) {
                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model->where('url', $url_new)->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        } else {
            $url_result_verify = $model
                ->where('url', $slugfy)
                ->where('id','<>' ,$id)
                ->first();
            if (count($url_result_verify) >= 1) {
                $return = false;
                $i = 1;
                do {
                    $url_new = $slugfy . '-' . $i++;
                    $url_result_verify = $model
                        ->where('url', $url_new)
                        ->where('id','<>' ,$id)
                        ->first();
                    if ($url_result_verify->url == "") {
                        $return = true;
                    }
                } while ($return == false);
                return strtolower($url_new);
                exit;
            } else {
                return strtolower($slugfy);
                exit;
            }
        }
    }

    //Remover hifens e espaços
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    //URL Amigável
    public function slugify($text){
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    //Cortar textos
    function excerpt($str, $startPos=0, $maxLength=100) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength-3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= '...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    //Verificar se key=>value existe em um array
    function in_array_key($array, $url, $method) {
        //Remove a ultima barra
        $url = rtrim($url, "/");
        foreach ($array as $key=>$item) {
            if ($key==$url and $item==$method) {
                return true;
                exit();
            }
        }
        return false;
    }

    public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';
        $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;
        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }
        return $retorno;
    }

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir."/".$object))
                        rrmdir($dir."/".$object);
                    else
                        unlink($dir."/".$object);
                }
            }
            rmdir($dir);
        }
    }

}
