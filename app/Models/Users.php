<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    protected $table = 'users';
    protected $guarded = [];
    
    protected $fillable = [
        'id', 'name', 'email', 'password', 'permission', 'profile', 'phone', 'cellphone', 'description'
    ];
    public $timestamps = true;

    public function logs()
    {
        return $this->hasMany('App\Models\Logs', 'users_id');
    }
}
