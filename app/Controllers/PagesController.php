<?php

namespace App\Controllers;

use App\Controllers\AppController;


class PagesController extends AppController
{
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index($req, $res)
    {
        $slogan = "Comece aqui";
        $this->view->render($res, 'index.twig', ['slogan' => $slogan]);
    }

    public function biblionat($req, $res){
        $this->view->render($res, 'biblionat.twig');
    }

    public function contato($req, $res){
        $this->view->render($res, 'contato.twig');
    }

    public function bionews($req, $res) {
        $this->view->render($res, 'bionews.twig');
    }

    public  function post($req, $res) {
        $this->view->render($res, 'post.twig');
    }

    public  function eventos($req, $res) {
        $this->view->render($res, 'eventos.twig');
    }

    public  function evento($req, $res) {
        $this->view->render($res, 'evento.twig');
    }

    public function bioforcas($req, $res) {
        $this->view->render($res, 'bioforcas.twig');
    }

    public function mip($req, $res) {
        $this->view->render($res, 'mip.twig');
    }







};
