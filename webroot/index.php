<?php
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
define('DS', DIRECTORY_SEPARATOR);
define('DS_URL', '/');
define('ROOT', dirname(__DIR__));
define('APP_DIR', 'app');
define('APP', ROOT . DS . APP_DIR );
define('CONFIG', ROOT . DS . 'config'.DS);
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);

//Iniciar sessão
if (file_exists(ROOT . DS . 'vendor' . DS . 'autoload.php')) {
    require ROOT . DS . 'vendor' . DS . 'autoload.php';
} else {
    echo "Execute o comando 'composer install'";
    exit();
}
$settings = require CONFIG . DS . 'settings.php';
$app = new \Slim\App($settings);
require CONFIG . DS . 'dependencies.php';
require CONFIG . DS . 'middleware.php';
require CONFIG . DS . 'routes.php';
$app->run();
